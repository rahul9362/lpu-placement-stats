var placementsData;

function getPlacementsData() {
    setTimeout(() => {
        var dataFetchUrl = "https://script.google.com/macros/s/AKfycbyLRic4AsszTWx5mqdNfq3MkVx1XnePfCJkA-o_AW6wSOLeXOXWZzfCcJf3JIXp-jLO9Q/exec";
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.open("GET", dataFetchUrl, false);
        xmlHttp.send(null);
        var response = JSON.parse(xmlHttp.responseText);
        if (response.success) {
            placementsData = response.data;
            createHTML();
            setTimeout(() => {
                $('.loader').removeClass('show');
                $('.section_block').removeClass('hide');
                createCarousel();
            }, 1000);
        } else {
            setTimeout(() => {
                document.getElementById('section_body').innerHTML = response.error;
                $('.loader').removeClass('show');
                $('.section_block').removeClass('hide');
            }, 100);
        }
    }, 10);
}

function createHTML() {
    let keys = Object.keys(placementsData);
    for (let i = 0; i < keys.length; i++) {
        let key = keys[i];
        let content = '';
        let dataSet = placementsData[key];
        for (let j = 0; j < dataSet.length; j++) {
            let card = getCardHtml(dataSet[j]);
            content += card;
        }
        if (key == 'gt30') {
            document.getElementById('placement1').innerHTML = content;
        } else if (key == 'gt20') {
            document.getElementById('placement2').innerHTML = content;
        } else if (key == 'gt10') {
            document.getElementById('placement3').innerHTML = content;
        }
    }
}

function getCardHtml(std) {
    let str = `<div class="item">
                <div class="placedStudentlist">
                    <div class="imgsec">
                        <img src="` + (std['photo'] ? 'assets/img/student/'+std['photo'] : 'assets/img/student/'+std['regNo']+'.jpg') + `" alt="` + std['name'] + `">
                    </div>
                    <div class="student_info">
                        <h5>` + std['name'] + `</h5>
                        <p>LPU Alumni, Class of ` + std['year'] + `</p>
                        <div class="companyPlaced">
                            <a href="javascript:void(0)"><img src="` + (std['logoUrl'] ? 'assets/img/LOGOS/'+std['logoUrl'] : 'assets/img/company.png') + `" alt="` + std['company'] + `" ></a> 
                        </div>
                    </div>
                </div>
            </div>`;
    return str;
}

function createCarousel() {
    $('#placement1').owlCarousel({
        margin: 10,
        nav: true,
    
        lazyLoad:true,
        navText: ["<img src='assets/img/back.svg'>", "<img src='assets/img/forward.svg'>"],
        responsiveClass:true,
    responsive:{
        0:{
            items:1,
            
        },
        440:{
            items:2,
           
        },
        600:{
            items:3,
           
        },
        768:{
            items:2,
           
        },
        900:{
            items:3,
           
        },
        1100:{
            items:4,
           
        },

    }
       
    })
    $('#placement2').owlCarousel({
        margin: 10,
        nav: true,
        lazyLoad:true,
      
        navText: ["<img src='assets/img/back.svg'>", "<img src='assets/img/forward.svg'>"],
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
                
            },
            440:{
                items:2,
               
            },
            600:{
                items:3,
               
            },
            768:{
                items:2,
               
            },
            900:{
                items:3,
               
            },
            1100:{
                items:4,
               
            },
      
        }
       
    })
    $('#placement3').owlCarousel({
        margin: 10,
        nav: true,
       
        navText: ["<img src='assets/img/back.svg'>", "<img src='assets/img/forward.svg'>"],
        lazyLoad:true,
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
                
            },
            440:{
                items:2,
               
            },
            600:{
                items:3,
               
            },
            768:{
                items:2,
               
            },
            900:{
                items:3,
               
            },
            1100:{
                items:4,
               
            },
        }
       
    })

}
$(document).ready(function () {
    $('.viewmore a').click(function () {
        if ($(this).hasClass('open')) {
            $(this).find('span').text('more')
        }
        else {
            $(this).find('span').text('less')
        }
        $(this).toggleClass('open');
        $('#placement3-outer').toggleClass('hide');

    });
});

getPlacementsData();