var schoolData;
var schoolNames;
var tilesData;
var chartData = {};
function schoolChanged(ev) {
    console.log(ev)
    getSchoolData(ev.target.value, false)
}
function getSchoolData(schInd, initDropdown) {
    document.getElementById('tab_content').innerHTML = `<div class="loader show">
        <div id="wave">
            <span class="dot"></span>
            <span class="dot"></span>
            <span class="dot"></span>
        </div>
    </div>`;
    document.getElementById('Statistics').innerHTML = `<div class="loader show">
        <div id="wave">
            <span class="dot"></span>
            <span class="dot"></span>
            <span class="dot"></span>
        </div>
    </div>`;
    setTimeout(() => {
        var dataFetchUrl = "https://script.google.com/macros/s/AKfycbzfl3RfaYE5nOQwbjGyihgx3n3UqHV9mW_kMhn0juJCFhQV7tAKyETDMQvGADg8gxtH/exec?schInd=" + schInd;
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.open("GET", dataFetchUrl, false);
        xmlHttp.send(null);
        var response = JSON.parse(xmlHttp.responseText);
        if (response.success) {
            schoolData = response.data['schoolData'];
            schoolNames = response.data['schoolNames'];
            tilesData = response.data['tilesData'];
            if (!tilesData.length) {
                jQuery('#Statistics').css('display', 'none')
            } else {
                jQuery('#Statistics').css('display', 'grid')
            }
            initDropdown && createDropDown(schInd);
            createHTML();
        } else {
            setTimeout(() => {
                document.getElementById('section_body').innerHTML = response.error;
            }, 100);
        }
        console.log();
    }, 10);
}

function createDropDown(schInd) {
    setTimeout(() => {
        for (var field in schoolNames) {
            $('<option' + (schInd - 1 == field ? ' selected' : '') + ' value="' + Number(Number(field) + 1) + '">' + schoolNames[field] + '</option>').appendTo('#select');
        }
    }, 100);
}

function createHTML() {
    // creating tabs
    var tabs = Object.keys(schoolData);
    let tabHtml = '';
    let tabInnerHtml = '';
    let tilesInnerHtml = '';
    for (var i = 0; i < tabs.length; i++) {
        tabHtml += `<li ` + (i == 0 ? `class="active"` : '') + `onclick='initCharts()' data-target="` + tabs[i] + `">` + tabs[i] + `</a></li>`
        tabInnerHtml += `<div class="tab ` + (i == 0 ? `active` : '') + `" id="` + tabs[i] + `">
                       <div class="table-content active `+ (i == 0 ? `active` : '') + `" id="table-view"> ` +
            getTableContent(tabs[i])
            + `
                       </div>
                       <div class="chart-content" id="chart-view">
                           <div class="chart_list">
                            ` + getChartContent(tabs[i]) + `
                        </div>
                       </div>
                   </div>
                   `;
    }
    tilesInnerHtml = getTilesContent();
    setTimeout(() => {
        document.getElementById('maintabs').innerHTML = tabHtml;
        document.getElementById('tab_content').innerHTML = tabInnerHtml;
        document.getElementById('Statistics').innerHTML = tilesInnerHtml;
        $('.tabs li').click(function () {
            $('.tabs li').removeClass('active');
            $('.tab').removeClass('active');
            $(this).addClass('active');
            var tabValue = $(this).attr('data-target');
            $('#' + tabValue).addClass('active');
        });
        $('.tableView').click(function () {
            $('.chartView').addClass('active');
            $(this).removeClass('active');
            $('.chart-content').addClass('active');
            $('.table-content').removeClass('active');
            initCharts();
        });
        $('.chartView').click(function () {
            $('.tableView').addClass('active');
            $(this).removeClass('active');
            $('.chart-content').removeClass('active');
            $('.table-content').addClass('active');
        });
    }, 100);

}
function getTableContent(tabName) {
    let data = schoolData[tabName];
    let rows = Object.keys(data);
    let years = Object.keys(data[rows[0]]);
    let yearsRow = '';
    let tableRows = '';
    for (let i = 0; i < years.length; i++) {
        yearsRow += `<th>` + years[i] + `</th>`
    }
    for (let i = 0; i < rows.length; i++) {
        let tableData = '';
        let column = data[rows[i]];
        for (let j = 0; j < Object.keys(column).length; j++) {
            tableData += `<td>` + column[Object.keys(column)[j]] + `</td>`
        }
        tableRows += `<tr >
                                <td>` + rows[i] + `</td>
                                        ` + tableData + `
                                    </tr>`;
    }
    return `<table class="table">
                                <thead>
                                    <tr>
                                        <th></th>
                                        ` + yearsRow + `
                                    </tr>
                                </thead>
                                <tbody>
                                    ` + tableRows + `
                                </tbody>
                           </table>`
}

function getChartContent(tabName) {
    let chartContent = '';
    chartData[tabName] = {};
    let data = schoolData[tabName];
    let rows = Object.keys(data);
    let years = Object.keys(data[rows[0]]);
    for (let i = 0; i < rows.length; i++) {
        let tableData = '';
        chartData[tabName][rows[i]] = { years: Object.keys(filterEmptyCols(data[rows[i]])), values: Object.values(filterEmptyCols(data[rows[i]])) }
        chartContent += `<div class="chart">
                                   <h5> ` + rows[i] + `</h5>
                                   <div class="chart-body" id="` + rows[i] + `">
									<canvas class="stat_chart" id="stat_chart` + rows[i] + `" style="height:300px;width:300px;"></canvas>
                                   </div>
                               </div>`;
    }
    return chartContent;
}

function filterEmptyCols(data) {
    let obj = {};
    let keys = Object.keys(data);
    for (i = 0; i < keys.length; i++) {
        if (data[keys[i]] != '' && data[keys[i]] != 0)
            obj[keys[i]] = data[keys[i]];
    }
    return obj;
}

function getTilesContent() {
    let content = '';
    for (let i = 0; i < tilesData.length; i++) {
        content += `<div class="statistics_block">
                        <h5>` + tilesData[i].value + `</h5>
                        <p>` + tilesData[i].key + `</p>
                    </div>`;
    }
    return content;
}


function initCharts() {
    /*Chart Code Start*/
    setTimeout(() => {
        let chartElements = jQuery('.tab.active .chart-content.active .chart-body');
        chartElements.each(function (index) {
            let id = jQuery(this).attr('id');
            let tab = jQuery('.tab.active').attr('id')
            let years = chartData[tab][id]['years'];
            let values = chartData[tab][id]['values'].map(e => Number(e.toString().replace(/[^0-9.]/g, '')))
            var ctx = document.getElementById('stat_chart' + id).getContext("2d");

            /*** Gradient ***/
            var gradient = ctx.createLinearGradient(0, 0, 0, 150);
            gradient.addColorStop(0, 'rgba(253,200,48,1)');
            gradient.addColorStop(1, 'rgba(243,115,53,0.1)');

            /***************/

            var data = {
                labels: years,
                datasets: [
                    {
                        label: 'LPA',
                        fillColor: gradient, // Put the gradient here as a fill color
                        strokeColor: "#ff6c23",
                        pointColor: "#fff",
                        pointStrokeColor: "#ff6c23",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "#ff6c23",
                        data: values
                    }
                ]
            };
            var options = {
                responsive: false,
                datasetStrokeWidth: 2,
                pointDotStrokeWidth: 2,
                tooltipFillColor: "rgba(0,0,0,0.8)",
                tooltipFontStyle: "normal"
            };
            var myLineChart = new Chart(ctx).Line(data, options);
        }, 100);
        /*Chart Code End*/
    });
}

const params = new Proxy(new URLSearchParams(window.location.search), {
    get: (searchParams, prop) => searchParams.get(prop),
});
let schoolName = params['schInd'];
getSchoolData(schoolName, true);